#pragma once

#include "Vec3.h"


//*****************************************************************************
// Vec4
//*****************************************************************************
class Vec4 {
public:
	float x;
	float y;
	float z;
	float w;

	Vec4();
	Vec4(const float X, const float Y, const float Z, const float W);

	void Set(const float X, const float Y, const float Z, const float W);
	void Set(const Vec3 &XYZ, const float W);

	float & operator [] (const int index);
	float operator [] (const int index) const;
};
