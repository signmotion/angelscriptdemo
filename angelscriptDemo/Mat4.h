#pragma once

#include "Vec3.h"
#include "Vec4.h"


//*****************************************************************************
// Mat4
//*****************************************************************************
class Mat4 {
public:
	Mat4();
	Mat4(const Vec3 &scaling, const Vec3 &translation);
	Mat4(const float _00, const float _01, const float _02, const float _03,
		const float _10, const float _11, const float _12, const float _13,
		const float _20, const float _21, const float _22, const float _23,
		const float _30, const float _31, const float _32, const float _33);
	
	float get(int row, int col) const;
	float operator () (int row, int Col) const;
	float& operator () (int row, int col);

private:
	Vec4 rows[4];
};




// for AngelScript
// @source https://gist.github.com/MartinBspheroid/10103958
class RegisterMat4Helper {
public:
	static void opConstruct( Mat4* in ) { new (in) Mat4;  }
	static void opConstructInit1( const Vec3& scaling, const Vec3& translation, Mat4* in ) { new (in) Mat4( scaling, translation ); }
	static void opConstructInit2(
		float _00, float _01, float _02, float _03,
		float _10, float _11, float _12, float _13,
		float _20, float _21, float _22, float _23,
		float _30, float _31, float _32, float _33,
		Mat4* in ) {
			new (in) Mat4(
				_00, _01, _02, _03,
				_10, _11, _12, _13,
				_20, _21, _22, _23,
				_30, _31, _32, _33 );
	}
	static void opDestruct( Mat4* in ) { in->~Mat4(); }
	static Mat4& opAssign( const Mat4& rhs, Mat4* lhs ) { *lhs = rhs; return *lhs; }
};




std::ostream& operator<<( std::ostream&,  const Mat4& );
