#include "stdafx.h"
#include "Mat4.h"




Mat4::Mat4() {
}


Mat4::Mat4(const Vec3 &scaling, const Vec3 &translation) {
	rows[0].Set(scaling.x, 0.0f, 0.0f, 0.0f);
	rows[1].Set(0.0f, scaling.y, 0.0f, 0.0f);
	rows[2].Set(0.0f, 0.0f, scaling.z, 0.0f);
	rows[3].Set(translation, 1.0f);
}


Mat4::Mat4(const float _00, const float _01, const float _02, const float _03,
					const float _10, const float _11, const float _12, const float _13,
					const float _20, const float _21, const float _22, const float _23,
					const float _30, const float _31, const float _32, const float _33) {
	rows[0].Set(_00, _01, _02, _03);
	rows[1].Set(_10, _11, _12, _13);
	rows[2].Set(_20, _21, _22, _23);
	rows[3].Set(_30, _31, _32, _33);
}


float Mat4::get(int row, int col) const {
	assert(row >= 0 && row < 4);
	assert(col >= 0 && col < 4);
	return rows[row][col];
}


float& Mat4::operator () (int row, int col) {
	assert(row >= 0 && row < 4 && col >= 0 && col < 4);
	return rows[row][col];
}


float Mat4::operator () (int row, int col) const {
	assert(row >= 0 && row < 4 && col >= 0 && col < 4);
	return rows[row][col];
}




std::ostream& operator<<( std::ostream& out,  const Mat4& m ) {
	out << "| " << m.get( 0, 0 ) << "\t" << m.get( 0, 1 ) << "\t" << m.get( 0, 2 ) << "\t" << m.get( 0, 3 ) << " |\n"
	       "| " << m.get( 1, 0 ) << "\t" << m.get( 1, 1 ) << "\t" << m.get( 1, 2 ) << "\t" << m.get( 1, 3 ) << " |\n"
	       "| " << m.get( 2, 0 ) << "\t" << m.get( 2, 1 ) << "\t" << m.get( 2, 2 ) << "\t" << m.get( 2, 3 ) << " |\n"
	       "| " << m.get( 3, 0 ) << "\t" << m.get( 3, 1 ) << "\t" << m.get( 3, 2 ) << "\t" << m.get( 3, 3 ) << " |\n";
	return out;
}
