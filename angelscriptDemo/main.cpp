#include "stdafx.h"
#include "Mat4.h"
#include "Vec3.h"


int run();
void messageCallback( const asSMessageInfo*, void* param );
void print( const std::string& text );
void print( const std::string& text, const std::string& header );
void print( float v, const std::string& name );
void print( const Vec3&, const std::string& name );
void print( const Mat4&, const std::string& name );


class RegisterMat4Helper;
class RegisterVec3Helper;




// AngelScript helpers.
namespace angelscripthelper {

void rtype( const char* obj, int byteSize, asDWORD flags );
void rprop( const char* obj, const char* declaration, int byteOffset );
void rmethod( const char* obj, const char* declaration, const asSFuncPtr& funcPointer, asDWORD callConv );
void rbeh( const char* obj, asEBehaviours behaviour, const char* declaration, const asSFuncPtr& funcPointer, asDWORD callConv );

// The macros for simplest registration.
// @example
//		R( Vox, add, void, (const string& in) const );
//      instead of ==>
//		rmethod( "Vox", "void add( const string& in ) const", asFUNCTION( RegisterVoxHelper::add ) );
#define R(className,methodName,methodReturn,methodDecl)  rmethod( \
										#className, \
										#methodReturn" "#methodName#methodDecl, \
										asFUNCTION( Register##className##Helper::##methodName ) )

} // angelscripthelper




asIScriptEngine* engine;



int
main( int argc, char** argv ) {

    setlocale( LC_ALL, "Russian" );
    // ��� ����������� '.' ������ ','
    setlocale( LC_NUMERIC, "C" );

    int code = 0;
    try {
        code = run();
    } catch ( ... ) {
        code = INT_MIN;
    }

    std::cout << "\n^\n";
    std::cin.ignore();

    return code;

} // main()




int
run() {

    using namespace angelscripthelper;

    // Create the script engine
    engine = asCreateScriptEngine( ANGELSCRIPT_VERSION );
    // Set the message callback to receive information on errors in human readable form.
    int r = engine->SetMessageCallback( asFUNCTION( messageCallback ), 0, asCALL_CDECL );
    assert( r >= 0 );
    // AngelScript doesn't have a built-in string type, as there is no definite standard 
    // string type for C++ applications. Every developer is free to register it's own string type.
    // The SDK do however provide a standard add-on for registering a string type, so it's not
    // necessary to implement the registration yourself if you don't want to.
    RegisterStdString( engine );
    RegisterScriptMath( engine );
    RegisterScriptMathComplex( engine );
    RegisterScriptFile( engine );

    // Register the function that we want the scripts to call 
    r = engine->RegisterGlobalFunction( "void print( const string &in )",
        asFUNCTIONPR( print, (const std::string&), void ), asCALL_CDECL );
    assert( r >= 0 );
    r = engine->RegisterGlobalFunction( "void print( const string &in, const string &in )",
        asFUNCTIONPR( print, (const std::string&, const std::string&), void ), asCALL_CDECL );
    assert( r >= 0 );
    r = engine->RegisterGlobalFunction( "void print( float, const string &in )",
        asFUNCTIONPR( print, (float, const std::string&), void ), asCALL_CDECL );
    assert( r >= 0 );


    // all types
    rtype( "Mat4", sizeof( Mat4 ),
        asOBJ_VALUE | asOBJ_APP_CLASS_CDA );
#if 1
    // asOBJ_VALUE
    rtype( "Vec3", sizeof( Vec3 ),
        asOBJ_VALUE | asOBJ_APP_CLASS_CDA );
#else
    // asOBJ_REF
    rtype( "Vec3", sizeof( Vec3 ),
        asOBJ_REF );
#endif


    // Mat4
    rbeh( "Mat4", asBEHAVE_CONSTRUCT,
        "void f()", asFUNCTION( RegisterMat4Helper::opConstruct ), asCALL_CDECL_OBJLAST );
    rbeh( "Mat4", asBEHAVE_CONSTRUCT,
        "void f( "
        "float v00, float v01, float v02, float v03, "
        "float v10, float v11, float v12, float v13, "
        "float v20, float v21, float v22, float v23, "
        "float v30, float v31, float v32, float v33 )",
        asFUNCTION( RegisterMat4Helper::opConstructInit2 ), asCALL_CDECL_OBJLAST );
    rbeh( "Mat4", asBEHAVE_DESTRUCT,
        "void f()", asFUNCTION( RegisterMat4Helper::opDestruct ), asCALL_CDECL_OBJLAST );

    // methods
    rmethod( "Mat4", "Mat4& opAssign( const Mat4& in )",
        asFUNCTION( RegisterMat4Helper::opAssign ), asCALL_CDECL_OBJLAST );
    rmethod( "Mat4", "float get( int row, int col ) const",
        asMETHOD( Mat4, get ), asCALL_THISCALL );
    rbeh( "Mat4", asBEHAVE_CONSTRUCT,
        "void f( const Vec3 &in, const Vec3 &in )",
        asFUNCTION( RegisterMat4Helper::opConstructInit1 ), asCALL_CDECL_OBJLAST );

    // out
    r = engine->RegisterGlobalFunction( "void print( const Mat4 &in, const string &in )",
        asFUNCTIONPR( print, (const Mat4&, const std::string&), void ), asCALL_CDECL );
    assert( r >= 0 );


    // Vec3
#if 1
    // asOBJ_VALUE
    rbeh( "Vec3", asBEHAVE_CONSTRUCT,
        "void f()", asFUNCTION( RegisterVec3Helper::opConstruct ), asCALL_CDECL_OBJLAST );
    rbeh( "Vec3", asBEHAVE_CONSTRUCT,
        "void f( float x, float y, float z )", asFUNCTION( RegisterVec3Helper::opConstructInit1 ), asCALL_CDECL_OBJLAST );
    rbeh( "Vec3", asBEHAVE_DESTRUCT,
        "void f()", asFUNCTION( RegisterVec3Helper::opDestruct ), asCALL_CDECL_OBJLAST );
    // @see http://angelcode.com/angelscript/sdk/docs/manual/doc_script_class_ops.html
    rmethod( "Vec3", "Vec3& opAssign( const Vec3& in )",
        asFUNCTION( RegisterVec3Helper::opAssign ), asCALL_CDECL_OBJLAST );
#else
    // asOBJ_REF
    rbeh( "Vec3", asBEHAVE_FACTORY,
        "Vec3 @f()", asFUNCTION( RegisterVec3Helper::factory ), asCALL_CDECL );
    rbeh( "Vec3", asBEHAVE_FACTORY,
        "Vec3 @f()", asFUNCTION( RegisterVec3Helper::factoryXYZ ), asCALL_CDECL );
    rbeh( "Vec3", asBEHAVE_ADDREF,
        "void f()", asMETHOD( Vec3, addRef ), asCALL_THISCALL );
    rbeh( "Vec3", asBEHAVE_RELEASE,
        "void f()", asMETHOD( Vec3, release ), asCALL_THISCALL );
#endif

    rmethod( "Vec3", "Vec3& opAddAssign( const Vec3 &in )",
        asMETHOD( Vec3, operator+= ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3& opSubAssign( const Vec3 &in )",
        asMETHOD( Vec3, operator-= ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3& opMulAssign( const Vec3 &in )",
        asMETHODPR( Vec3, operator*=, (const Vec3&), void ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3& opMulAssign( float n )",
        asMETHODPR( Vec3, operator*=, (float), void ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3& opDivAssign( const Vec3 &in )",
        asMETHODPR( Vec3, operator/=, (const Vec3&), void ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3& opDivAssign( float n )",
        asMETHODPR( Vec3, operator/=, (float), void ), asCALL_THISCALL );

    rmethod( "Vec3", "Vec3 opAdd( const Vec3 &in ) const",
        asMETHODPR( Vec3, operator+, (const Vec3&) const, Vec3 ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3 opSub( const Vec3 &in ) const",
        asMETHODPR( Vec3, operator-, (const Vec3&) const, Vec3 ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3 opMul( const Vec3 &in ) const",
        asMETHODPR( Vec3, operator*, (const Vec3&) const, Vec3 ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3 opMul( float n ) const",
        asMETHODPR( Vec3, operator*, (float) const, Vec3 ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3 opDiv( const Vec3 &in ) const",
        asMETHODPR( Vec3, operator/, (const Vec3&) const, Vec3 ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3 opDiv( float n ) const",
        asMETHODPR( Vec3, operator/, (float) const, Vec3 ), asCALL_THISCALL );

    // methods
    rmethod( "Vec3", "float length() const",
        asMETHODPR( Vec3, length, () const, float ), asCALL_THISCALL );
    rmethod( "Vec3", "Vec3 transformCoordinate( const Mat4 &in )",
        asMETHOD( Vec3, transformCoordinate ), asCALL_THISCALL );

    // properties
    rprop( "Vec3", "float x", asOFFSET( Vec3, x ) );
    rprop( "Vec3", "float y", asOFFSET( Vec3, y ) );
    rprop( "Vec3", "float z", asOFFSET( Vec3, z ) );

    // out
    r = engine->RegisterGlobalFunction( "void print( const Vec3 &in, const string &in )",
        asFUNCTIONPR( print, (const Vec3&, const std::string&), void ), asCALL_CDECL );
    assert( r >= 0 );


    // The CScriptBuilder helper is an add-on that loads the file,
    // performs a pre-processing pass if necessary, and then tells
    // the engine to build a script module.
    CScriptBuilder builder;
    r = builder.StartNewModule( engine, "MyModule" );
    if ( r < 0 ) {
        // If the code fails here it is usually because there
        // is no more memory to allocate the module
        printf( "Unrecoverable error while starting a new module.\n" );
        return -1;
    }

    r = builder.AddSectionFromFile( "resources/test.as" );
    //const char*  text = "void main() { print( \"AngelScript demo\" ); }";
    //r = builder.AddSectionFromMemory( "MyModule", text );
    if ( r < 0 ) {
        // The builder wasn't able to load the file. Maybe the file
        // has been removed, or the wrong name was given, or some
        // preprocessing commands are incorrectly written.
        printf( "Please correct the errors in the script and try again.\n" );
        return -2;
    }

    r = builder.BuildModule();
    if ( r < 0 ) {
        // An error occurred. Instruct the script writer to fix the 
        // compilation errors that were listed in the output stream.
        printf( "Please correct the errors in the script and try again.\n" );
        return -3;
    }


    // Find the function that is to be called. 
    asIScriptModule* mod = engine->GetModule( "MyModule" );
    asIScriptFunction* func = mod->GetFunctionByDecl( "void main()" );
    if ( !func ) {
        // The function couldn't be found. Instruct the script writer
        // to include the expected function in the script.
        printf( "The script must have the function 'void main()'. Please add it and try again.\n" );
        return -4;
    }

    // Create our context, prepare it, and then execute
    asIScriptContext* ctx = engine->CreateContext();
    ctx->Prepare( func );
    r = ctx->Execute();
    if ( r != asEXECUTION_FINISHED ) {
        // The execution didn't complete as expected. Determine what happened.
        if ( r == asEXECUTION_EXCEPTION ) {
            // An exception occurred, let the script writer know what happened so it can be corrected.
            printf( "An exception '%s' occurred. Please correct the code and try again.\n", ctx->GetExceptionString() );
        }
    }

    // Clean up
    ctx->Release();
    engine->ShutDownAndRelease();

    return 0;
}




// Implement a simple message callback function
void
messageCallback( const asSMessageInfo* msg, void* param ) {
  const char* type = "ERR ";
  if ( msg->type == asMSGTYPE_WARNING ) 
    type = "WARN";
  else if ( msg->type == asMSGTYPE_INFORMATION ) 
    type = "INFO";
  printf("%s (%d, %d) : %s : %s\n", msg->section, msg->row, msg->col, type, msg->message);
}




// Print the script string to the standard output stream
void
print( const std::string& text ) {
    printf( "%s\n", text.c_str() );
}


void
print( const std::string& text, const std::string& header ) {
    printf( "'%s'\t%s\n", header.c_str(), text.c_str() );
}


void
print( float v, const std::string& name ) {
	std::cout << "'" << name << "'\t" << v << "\n";
}


void print( const Vec3& v, const std::string& name ) {
	std::cout << "'" << name << "'\t" << v << "\n";
}


void
print( const Mat4& m, const std::string& name ) {
	std::cout << "'" << name << "'\n" << m << "\n";
}




namespace angelscripthelper {

void
rtype( const char* obj, int byteSize, asDWORD flags ) {
	const int r =
		engine->RegisterObjectType( obj, byteSize, flags );
	if ( r < 0 ) {
		const bool test = true;
	}
	assert( r >= 0 );	
}


void
rprop( const char* obj, const char* declaration, int byteOffset ) {
	const int r =
		engine->RegisterObjectProperty( obj, declaration, byteOffset );
	if ( r < 0 ) {
		const bool test = true;
	}
	assert( r >= 0 );	
}


void
rmethod(
    const char* obj,
    const char* declaration,
    const asSFuncPtr& funcPointer,
    asDWORD callConv
) {
	const int r =
		engine->RegisterObjectMethod( obj, declaration, funcPointer, callConv );
	if ( r < 0 ) {
		const bool test = true;
	}
	assert( r >= 0 );	
}


void
rbeh( const char* obj, asEBehaviours behaviour, const char* declaration, const asSFuncPtr& funcPointer, asDWORD callConv ) {
	const int r =
		engine->RegisterObjectBehaviour( obj, behaviour, declaration, funcPointer, callConv );
	if ( r < 0 ) {
		const bool test = true;
	}
	assert( r >= 0 );	
}

} // angelscripthelper
