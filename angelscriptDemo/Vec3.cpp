#include "stdafx.h"
#include "Vec3.h"
#include "stdafx.h"
#include "Mat4.h"




Vec3::Vec3() {
}


Vec3::Vec3( float x, float y, float z )
    : x( x ), y( y ), z( z ) {}


void Vec3::addRef() const {
    asAtomicInc( mRefCount );
}


void Vec3::release() const {
    if ( asAtomicDec( mRefCount ) == 0 ) {
        delete this;
    }
}


void Vec3::operator += (const Vec3 &u) {
	x += u.x;
	y += u.y;
	z += u.z;
}


void Vec3::operator -= (const Vec3 &u) {
	x -= u.x;
	y -= u.y;
	z -= u.z;
}


void Vec3::operator *= (const Vec3 &u) {
	x *= u.x;
	y *= u.y;
	z *= u.z;
}


void Vec3::operator *= (float s) {
	x *= s;
	y *= s;
	z *= s;
}


void Vec3::operator /= (const Vec3 &u) {
	x /= u.x;
	y /= u.y;
	z /= u.z;
}


void Vec3::operator /= (float s) {
	float is = 1.0f / s;
	x *= is;
	y *= is;
	z *= is;
}


Vec3 Vec3::operator + (const Vec3 &u) const {
	return Vec3(x + u.x, y + u.y, z + u.z);
}


Vec3 Vec3::operator - (const Vec3 &u) const {
	return Vec3(x - u.x, y - u.y, z - u.z);
}


Vec3 Vec3::operator * (const Vec3 &u) const {
	return Vec3(x * u.x, y * u.y, z * u.z);
}


Vec3 Vec3::operator * (float s) const {
	return Vec3(x * s, y * s, z * s);
}


Vec3 operator * (float s, const Vec3 &u) {
	return Vec3(s * u.x, s * u.y, s * u.z);
}


Vec3 Vec3::operator / (const Vec3 &u) const {
	return Vec3(x / u.x, y / u.y, z / u.z);
}


Vec3 Vec3::operator / (float s) const {
	float is = 1.0f / s;
	return Vec3(x * is, y * is, z * is);
}


Vec3 Vec3::transformCoordinate(const Mat4 &T) {
	Vec3 r;
	r.x = x * T(0, 0) + y * T(1, 0) + z * T(2, 0) + T(3, 0);
	r.y = x * T(0, 1) + y * T(1, 1) + z * T(2, 1) + T(3, 1);
	r.z = x * T(0, 2) + y * T(1, 2) + z * T(2, 2) + T(3, 2);
	float w = x * T(0, 3) + y * T(1, 3) + z * T(2, 3) + T(3, 3);
	if (w != 1.0f) {
		float iw = 1.0f / w;
		r.x *= iw;
		r.y *= iw;
		r.z *= iw;
	}
	*this = r;
    return *this;
}


float Vec3::length() const {
	return sqrt(x * x + y * y + z * z);	
}




std::ostream& operator<<( std::ostream& out,  const Vec3& v ) {
	out << "[ " << v.x << " " << v.y << " " << v.z << " ]";
	return out;
}
