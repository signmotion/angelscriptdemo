#pragma once

#include <iostream>


class Mat4;


//*****************************************************************************
// Vec3
//*****************************************************************************
class Vec3 {
public:
	float x;
	float y;
	float z;

	Vec3();
    Vec3( float x, float y, float z );
    virtual ~Vec3() {}

    void addRef() const;
    void release() const;

	void operator += (const Vec3 &);
	void operator -= (const Vec3 &);
	void operator *= (const Vec3 &);
	void operator *= (float);
	void operator /= (const Vec3 &);
	void operator /= (float);
	
	Vec3 operator + (const Vec3 &) const;
	Vec3 operator - (const Vec3 &) const;
	Vec3 operator * (const Vec3 &) const;
	Vec3 operator * (float) const;
	Vec3 operator / (const Vec3 &) const;
	Vec3 operator / (float) const;

	Vec3 transformCoordinate(const Mat4 &);

	float length() const;

private:
    mutable int  mRefCount;
};




// for AngelScript
// @source https://gist.github.com/MartinBspheroid/10103958
class RegisterVec3Helper {
public:
#if 1
    // asOBJ_VALUE
	static void opConstruct( Vec3* in ) { new (in) Vec3;  }
	static void opConstructInit1( float x, float y, float z, Vec3* in ) { new (in) Vec3( x, y, z );  }
	static void opDestruct( Vec3* in ) { in->~Vec3(); }
	static Vec3& opAssign( const Vec3& rhs, Vec3* lhs ) { *lhs = rhs; return *lhs; }
#else
    // asOBJ_REF
    static Vec3* factory() { return new Vec3; }
    static Vec3* factoryXYZ( float x, float y, float z ) { return new Vec3( x, y, z ); }
#endif
};




std::ostream& operator<<( std::ostream&,  const Vec3& );
