void main() {

    print( "AngelScript demo" );
    
    
    // ScriptFile
    file  f;
    const int  rf = f.open( "resources/test.as", "r" );
    print( rf + "", "f.open()" );
    file@  pf = f;
    const int  rpf = pf.close();
    print( rpf + "", "pf.close()" );
    
    
    // complex
    complex  e = { 1.1, 2.2 };
    //complex@ pe = e;
    //print( e.r + " " + e.i, "complex" );
    
    
    // Vec3
    Vec3 a;
    a.x = 10.1;
    a.y = -50.5;
    a.z = 90.9;
    print( a.x + " " + a.y + " " + a.z, "a" );

    Vec3 b( 9, 49, 89 );
    print( b.x + " " + b.y + " " + b.z, "b" );

    Vec3 c = b;
    print( c.x + " " + c.y + " " + c.z, "c = b" );

    a += b;
    print( a.x + " " + a.y + " " + a.z, "a += b" );
    a -= b;
    print( a.x + " " + a.y + " " + a.z, "a -= b" );

    a *= b;
    print( a.x + " " + a.y + " " + a.z, "a *= b" );
    a /= b;
    print( a.x + " " + a.y + " " + a.z, "a /= b" );

    a *= 1.2;
    print( a.x + " " + a.y + " " + a.z, "a *= 1.2" );
    a /= 1.2;
    print( a.x + " " + a.y + " " + a.z, "a /= 1.2" );

    Vec3 d = a + b;
    print( d.x + " " + d.y + " " + d.z, "a + b" );
    d = a - b;
    print( d.x + " " + d.y + " " + d.z, "a - b" );
    d = a * b;
    print( d.x + " " + d.y + " " + d.z, "a * b" );
    d = a * 500;
    print( d.x + " " + d.y + " " + d.z, "a * 500" );
    d = a / b;
    print( d.x + " " + d.y + " " + d.z, "a / b" );
    d = a / 500;
    print( d.x + " " + d.y + " " + d.z, "a / 500" );

    print( " " + a.length(), "a.length()" );
    
    //Vec3@  p = a;
    //print( p.x + " " + p.y + " " + p.z, "Vec3@ p = a" );
    
    
    // Mat4
    Vec3 scaling( 2, 5, 6 );
    Vec3 translation( -200, -50, 600 );
    Mat4 ma( scaling, translation );
    print( ma, "ma" );

    Mat4 mb(
        2, 0, 0,  1,
        0, 3, 0,  2,
        0, 0, 1, -1,
        0, 0, 0,  1 );
    print( mb, "mb" );
    
    
    // Vec3 with Mat4
    a.transformCoordinate( ma );
    print( a, "a after transform" );
    
    
    // Vec3 with double Mat4
    a.transformCoordinate( ma ).transformCoordinate( ma );
    print( a, "a after double transform" );
}
