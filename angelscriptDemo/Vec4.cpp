#include "stdafx.h"
#include "Vec4.h"




Vec4::Vec4() {
}


Vec4::Vec4(const float X, const float Y, const float Z, const float W)
: x(X), y(Y), z(Z), w(W) {}


void Vec4::Set(const float X, const float Y, const float Z, const float W) {
	x = X;
	y = Y;
	z = Z;
	w = W;
}


void Vec4::Set(const Vec3 &XYZ, const float W) {
	x = XYZ.x;
	y = XYZ.y;
	z = XYZ.z;
	w = W;
}


float & Vec4::operator [] (const int index) {
	assert(index >= 0 && index < 4);
	return (&x)[index];
}


float Vec4::operator [] (const int index) const {
	assert(index >= 0 && index < 4);
	return (&x)[index];
}
